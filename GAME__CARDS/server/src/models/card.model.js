import mongoose from 'mongoose'

const Schema = mongoose.Schema

const CardSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
  },

  attackValue: {
    type: Number,
    required: true,
  },

  hpValue: {
    type: Number,
    required: true,
  },

  spell: {
    type: String,
    required: true,
  },

  _id: {
    type: String,
  },
})

export default mongoose.model('Card', CardSchema)
