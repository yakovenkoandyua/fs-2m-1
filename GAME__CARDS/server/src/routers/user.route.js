import express from 'express'
import UserController from '../controllers/user.controller.js'

const UserRouter = express.Router()

UserRouter.post('/', UserController.save)
UserRouter.post('/login', UserController.login)

export default UserRouter
