import express from 'express'
import CardController from '../controllers/card.controller.js'

const CardRouter = express.Router()

CardRouter.get('/', CardController.getCards)
CardRouter.post('/', CardController.addCard)

export default CardRouter
