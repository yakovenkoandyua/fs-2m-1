// JS
import './js/'

// SCSS
import './assets/scss/index.scss'

import { svg } from './js/svg'

window.addEventListener('load', () => {
  const allLinks = [...document.getElementsByTagName('a')].filter(
    item => item.dataset.btn
  )
  allLinks.forEach(link => {
    link.insertAdjacentHTML(
      'afterbegin',
      `${svg[link.dataset.btn].right}${svg[link.dataset.btn].left}`
    )
  })

  if (sessionStorage.getItem('token')) {
    document.querySelector('.header__button-login').style.display = 'none'
  } else {
    document.querySelector('.header__svg-profile').style.display = 'none'
  }
})

// const promise = new Promise(function(resolve, reject) {
//   setTimeout(() => {
//     console.log('request is send')
//     const data = {
//       user: 'andy',
//       age: 26,
//       like: 90
//     }
//     resolve(data)
//   }, 2000)
//   reject('data is not valid')
// })

// promise.then(value => {
//   console.log(value)
// })

// const baseUrl = 'https://jsonplaceholder.typicode.com/posts/1'

// let request = new XMLHttpRequest()

// request.open('GET', baseUrl)

// request.onload = function() {
//   console.log(JSON.parse(request.response))
// }

// request.send()

// GET - получить инфу
// POST - добавить информацию в базу, или проверка
// PUT - изменять данные
// DELETE - удаляет инфу с бэкенда
