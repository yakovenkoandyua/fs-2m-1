import './js/'
// SCSS
import './assets/scss/index.scss'

import FormSignIn from './js/components/formSignIn/FormSignIn'
import FormSignUp from './js/components/formSignUp/FormSignUp'

const formWrapper = document.querySelector('.forms__wrapper')
const formSignIn = new FormSignIn({
  id: 'formSignIn',
  className: 'forms__wrapper--signin'
})
const formSignUp = new FormSignUp({
  id: 'formSignUp',
  className: 'forms__wrapper--signup'
})

formWrapper.append(formSignIn.render(), formSignUp.render())

// ***** TABS FORM ***** //
function handleForm() {
  const svg = `<svg class="forms__wrapper--signup-svg" width="15px" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" class="svg-inline--fa fa-times fa-w-11" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg>`

  const formSignUp = document.getElementById('formSignUp')
  formSignUp.insertAdjacentHTML('beforeend', svg)
  // const tab = document.querySelector('.forms__tab')
  // const tabsTitles = document.querySelectorAll('.forms__tab--text')
  // const formSignIn = document.getElementById('formSignIn')
  formSignUp.addEventListener('click', function (e) {
    // tabsTitles.forEach((title, index) => {
    if (e.currentTarget === e.target) {
      // formSignUp.style.transform = 'translateX(-27%)'
      formSignUp.classList.add('active')
    }
    if (
      e.target.classList.contains('forms__wrapper--signup-svg') ||
      e.target.localName === 'path'
    ) {
      // formSignUp.style.transform = 'translateX(65%)'
      formSignUp.classList.remove('active')
    }

    // })

    // if (e.target.textContent === 'Sign Up') {
    //   formSignIn.style.transform = 'translateX(-280%)'
    // } else {
    //   formSignUp.style.transform = 'translateX(280%)'
    //   formSignIn.style.transform = 'translateX(50%)'
    // }

    // e.target.classList.add('active')
  })
}
handleForm()

// ***** REQUESTS  ***** //
const signUpSubmit = document.getElementById('signup')

signUpSubmit.addEventListener('click', e => {
  e.preventDefault()
  formSignUp.request()
})

const signInSubmit = document.getElementById('signin')
signInSubmit.addEventListener('click', e => {
  e.preventDefault()
  formSignIn.request()
})
