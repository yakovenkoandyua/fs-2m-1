export default class Api {
  static BASE_URL = 'http://localhost:8080/api'

  checkPath = (path, name) =>
    name ? `${Api.BASE_URL}/${path}/${name}` : `${Api.BASE_URL}/${path}`

  async getRequest(path, name = '') {
    const url = this.checkPath(path, name)

    const response = await fetch(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    })

    if (!response.ok) {
      const error = new Error('Error fetching the data.')
      error.info = await response.json()
      error.status = response.status
      throw error
    }
    return await response.json()
  }

  async editRequest(method, body, path, name) {
    const url = this.checkPath(path, name)
    console.log("🚀 ==== > Api ==== > url", url);

    const response = await fetch(url, {
      method,
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body),
    })

    if (!response.ok) {
      const error = new Error('Error fetching the data.')
      error.info = await response.json()
      error.status = response.status
      throw error
    }
    return await response.json()
  }

  async deleteRequest(body, path, name) {
    const url = this.checkPath(path, name)

    const response = await fetch(url, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body) // ! need or not
    })

    if (!response.ok) {
      const error = new Error('Error fetching the data.')
      error.info = await response.json()
      error.status = response.status
      throw error
    }
    return await response.json()
  }
}

// try {
//   const request = new Api().getRequest('/products')

//   console.log(request.data)
// } catch {
//   console.log(error)
// }

// /cards
// GET(admin/user) - get all cards
// GET(user) + token - get user deck
// POST(admin) - add one cards
// PUT(admin) - edit cards
// DELETE(admin/user(deck)) - del cards

// /auth
// POST(admin/user) - registr user
// POST(admin/user) - login user
// DELETE (admin) - delete user
// PUT(admin) - banned user

// /carousel
// GET(user) - get info title or some text to slider


