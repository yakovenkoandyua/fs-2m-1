import * as THREE from 'three'
import {GLTFLoader} from 'three/examples/jsm/loaders/GLTFLoader';
// import './GLTFLoader';
let container, camera, renderer, scene, unit

export default function init(path) {
  container = document.querySelector('.card__3dmodel')
  console.log("🚀 ==== > container", container);

  // Create scene
  scene = new THREE.Scene()

  const fov = 20
  const aspect = container.clientWidth / container.clientHeight
  const near = 0.1
  const far = 500

  camera = new THREE.PerspectiveCamera(fov, aspect, near, far)

  camera.position.set(0, 1, 10)

  const ambient = new THREE.AmbientLight(0x404040, 3)
  scene.add(ambient)

  const light = new THREE.DirectionalLight(0xffffff, 3)

  light.position.set(10, 10, 10)
  scene.add(light)

  // Render
  renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true })
  renderer.setSize(container.clientWidth, container.clientHeight)
  renderer.setPixelRatio(window.devicePixelRatio)

  container.append(renderer.domElement)

  //Load model
  let loader = new GLTFLoader()
  loader.load(path, function(gltf) {
    scene.add(gltf.scene)
    unit = gltf.scene.children[0]
    animate()
    // renderer.render(scene, camera)
  })
}

function animate() {
  requestAnimationFrame(animate)

  unit.rotation.z += 0.01
  renderer.render(scene, camera)
}


// init('./model/dron/scene.gltf')
// init('./model/akiras_moto/scene.gltf')
// init('./model/phoenix_bird/scene.gltf')
// init('./model/free_cyberpunk_hovercar/scene.gltf')
//

// function onWindowResize () {
//     camera.aspect = container.clientWidth / container.clientHeight

//     camera.updateProjectMatrix()
//     renderer.setSize(container.clientWidth, container.clientHeight)
// }

// window.addEventListener('resize', onWindowResize)
