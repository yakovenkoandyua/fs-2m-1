export const svg = {
  a: {
    left: `<svg width="16" height="64" viewBox="0 0 20 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M4 0H20L20 64H4V48L0 44V20L4 16V0Z" fill="#010101"/>
            </svg>`,
    right: `<svg width="16" height="64" viewBox="0 0 20 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16 0H0L0 64H16V48L20 44V20L16 16V0Z" fill="#010101"/>
            </svg>`
  },
  b: {
    left: `<svg width="16" height="64" viewBox="0 0 16 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 0H16L16 64H0V48L4 44V20L0 16V0Z" fill="#010101"/>
            </svg>
`,
    right: `<svg width="16" height="64" viewBox="0 0 16 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16 0H0L0 64H16V48L12 44V20L16 16V0Z" fill="#010101"/>
            </svg>`
  },
  c: {
    left: `<svg width="16" height="64" viewBox="0 0 20 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M4 0H20L20 64H4V48L0 44V20L4 16V0Z" fill="#010101"/>
            </svg>`,
    right: `<svg width="16" height="64" viewBox="0 0 16 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16 0H0L0 64H16V48L12 44V20L16 16V0Z" fill="#010101"/>
            </svg>`
  },
  d: {
    left: `<svg width="16" height="64" viewBox="0 0 16 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 0H16L16 64H0V48L4 44V20L0 16V0Z" fill="#010101"/>
            </svg>`,
    right: `<svg width="16" height="64" viewBox="0 0 20 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16 0H0L0 64H16V48L20 44V20L16 16V0Z" fill="#010101"/>
            </svg>`
  },
  e: {
    left: `<svg width="16" height="64" viewBox="0 0 16 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16 0L0 15.7538V64H16V0Z" fill="#010101"/>
            </svg>`,
    right: `<svg width="16" height="64" viewBox="0 0 16 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16 0H0L0 64H16V48L12 44V20L16 16V0Z" fill="#010101"/>
            </svg>`
  },
  f: {
    left: `<svg width="16" height="64" viewBox="0 0 16 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16 64L0 48.2462V0H16V64Z" fill="#010101"/>
            </svg>`,
    right: `<svg width="16" height="64" viewBox="0 0 16 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16 0H0L0 64H16V48L12 44V20L16 16V0Z" fill="#010101"/>
            </svg>`
  },
  g: {
    left: `<svg width="16" height="64" viewBox="0 0 16 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 0H16L16 64H0V48L4 44V20L0 16V0Z" fill="#010101"/>
            </svg>`,
    right: `<svg width="16" height="64" viewBox="0 0 16 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 64L16 48.2462V0H0V64Z" fill="#010101"/>
            </svg>`
  },
  h: {
    left: `<svg width="16" height="64" viewBox="0 0 16 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 0H16L16 64H0V48L4 44V20L0 16V0Z" fill="#010101"/>
            </svg>`,
    right: `<svg width="16" height="64" viewBox="0 0 16 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 0L16 15.7538V64H0V0Z" fill="#010101"/>
            </svg>`
  },
  i: {
    left: `<svg width="16" height="64" viewBox="0 0 16 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16 0L0 15.7538V64H16V0Z" fill="#010101"/>
            </svg>`,
    right: `<svg width="16" height="64" viewBox="0 0 16 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 64L16 48.2462V0H0V64Z" fill="#010101"/>
            </svg>`
  },
  j: {
    left: `<svg width="16" height="64" viewBox="0 0 16 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16 64L0 48.2462V0H16V64Z" fill="#010101"/>
            </svg>`,
    right: `<svg width="16" height="64" viewBox="0 0 16 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 0L16 15.7538V64H0V0Z" fill="#010101"/>
            </svg>`
  },
  k: {
    left: `<svg width="16" height="64" viewBox="0 0 16 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16 64L0 48.2462V0H16V64Z" fill="#010101"/>
            </svg>`,
    right: `<svg width="16" height="64" viewBox="0 0 16 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 64L16 48.2462V0H0V64Z" fill="#010101"/>
            </svg>`
  },
  l: {
    left: `<svg width="16" height="64" viewBox="0 0 16 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16 0L0 15.7538V64H16V0Z" fill="#010101"/>
            </svg>`,
    right: `<svg width="16" height="64" viewBox="0 0 16 64" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 0L16 15.7538V64H0V0Z" fill="#010101"/>
            </svg>`
  }
}
