export default class Card {
  constructor({ ...props }) {
    this.props = props
  }

  static setValueCard(value, attribute) {
    const elementShowValue = document.getElementById(attribute)
    elementShowValue.textContent = value
  }

  setRarity(value) {
    const elementShowValue = document.querySelector('.card__rarity')
    elementShowValue.src = `./assets/img/${value}.png`
  }
}

// document.getElementById('select').addEventListener('click', ({ target }) => {
//   const card = new Card()
//   console.log(target.value)
//   card.setRarity(target.value)
// })
// setRole(value) {
//     const elementShowValue = document.querySelector('.card__role')
//     elementShowValue.textContent = value
//   }

//   setAttack(value) {
//     const elementShowValue = document.querySelector(
//       '.card__setting-attackValue'
//     )
//     elementShowValue.textContent = value
//   }

//   setHp(value) {
//     const elementShowValue = document.querySelector('.card__setting-hpValue')
//     elementShowValue.textContent = value
//   }

//   setName(value) {
//     const elementShowValue = document.querySelector('.card__setting-name')
//     elementShowValue.textContent = value
//   }

//   setSpell(value) {
//     const elementShowValue = document.querySelector('.card__spell')
//     elementShowValue.textContent = value
//   }