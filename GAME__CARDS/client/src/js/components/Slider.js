import Swiper, { Pagination } from 'swiper'

Swiper.use([Pagination])

const swiper = new Swiper('.swiper', {
  direction: 'horizontal',
  loop: true,
  autoplay: {
    delay: 2500,
    disableOnInteraction: false
  },
  pagination: {
    clickable: true,
    el: '.swiper-pagination'
  }
})
