import Form from '../../common/Form'
import Input from '../../common/Input'
import Api from '../../fether'
import { fields } from './helper'

export default class FormSignUp extends Form {
  constructor(data) {
    super(data)
    this.data = data
  }
  async request() {
    const dataField = this.serialize('.formCard__field')
    const { password, passwordConfirm } = dataField

    if (password === passwordConfirm) {
      const { user, token } = await new Api().editRequest(
        'POST',
        dataField,
        'users'
      )

      sessionStorage.setItem('token', token)
      window.location.href = 'index.html'
      
    } else {
      alert('пароли не совпадают')
    }
  }

  render() {
    const form = super.render(this.data)

    fields.forEach(field => {
      form.append(new Input(field).render())
    })

    return form
  }
}
