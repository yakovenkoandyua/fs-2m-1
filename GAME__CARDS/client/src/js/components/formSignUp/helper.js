export const fields = [
  {
    type: 'text',
    className: 'formCard__field',
    required: true,
    name: 'name',
    errorText: 'Корректно введите данные',
    placeholder: 'Nickname'
  },
  {
    type: 'email',
    className: 'formCard__field',
    required: true,
    name: 'email',
    errorText: 'Корректно введите данные',
    placeholder: 'Email'
  },
  {
    type: 'password',
    className: 'formCard__field',
    required: true,
    name: 'password',
    errorText: 'Корректно введите данные',
    placeholder: 'Password'
  },
  {
    type: 'password',
    className: 'formCard__field',
    // required: true,
    name: 'passwordConfirm',
    errorText: 'Корректно введите данные',
    placeholder: 'Confirm Password'
  },
  // {
  //   type: 'checkbox',
  //   className: 'formCard__field',
  //   required: true,
  //   name: 'check',
  //   id: 'check',
  //   label: {
  //     className: 'formCard__label',
  //     for: 'check',
  //     textContent: 'I Agree all ...'
  //   }
  // },
  {
    type: 'submit',
    id: 'signup',
    className: 'formCard__submit',
    value: 'Login'
  }
]
