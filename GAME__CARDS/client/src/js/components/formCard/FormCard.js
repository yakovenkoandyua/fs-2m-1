import Form from '../../common/Form'
import Input from '../../common/Input'
import Api from '../../fether'
import { fields } from './helper'

export default class FormCard extends Form {
  constructor(data) {
    super(data)
    this.data = data
  }

  async request() {
    const dataField = this.serialize('.formCard__field')


    const result = await new Api().editRequest('POST', dataField, 'card')
    
    console.log("🚀 ==== > FormCard ==== > result", result);
  }

  render() {

    const form = super.render(this.data)

    fields.forEach(field => {
      form.append(new Input(field).render(true))
    })

    return form
  }
}
