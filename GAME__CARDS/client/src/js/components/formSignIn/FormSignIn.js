import Form from '../../common/Form'
import Input from '../../common/Input'
import Api from '../../fether'
import { fields } from './helper'

export default class FormSignIn extends Form {
  constructor(data) {
    super(data)
    this.data = data
  }

  async request() {
    const dataField = this.serialize('.formCard__field')

    const { data, token } = await new Api().editRequest(
      'POST',
      dataField,
      'users',
      'login'
    )

    sessionStorage.setItem('token', token)
    window.location.href = 'index.html'
    
  }

  render() {
    const form = super.render(this.data)

    fields.forEach(field => {
      form.append(new Input(field).render())
    })

    return form
  }
}
