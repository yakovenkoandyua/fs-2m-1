export const fields = [
  {
    type: 'text',
    className: 'formCard__field',
    required: true,
    name: 'name',
    errorText: 'Корректно введите данные',
    placeholder: 'Nickname'
  },
  {
    type: 'password',
    className: 'formCard__field',
    required: true,
    name: 'password',
    errorText: 'Корректно введите данные',
    placeholder: 'Password'
  },
  {
    type: 'submit',
    className: 'formCard__submit',
    id: 'signin',
    value: 'Login'
  }
]
