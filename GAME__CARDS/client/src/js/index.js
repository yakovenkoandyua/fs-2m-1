import './components/Card'
import FormCard from './components/formCard/FormCard'
// import FormSignIn from './components/formSignIn/FormSignIn'
// import FormSignUp from './components/formSignUp/FormSignUp'
import Modal from './components/Modal'
import init from './3dmodel/app'
// import './3dmodel/GLTFLoader'
import './3dmodel/app'
import './components/Slider'
// ******* EXAMPLE ******* //
// function User(name){
//     this.name = name
// }

// new User('andy')

// class User {
//     constructor(name) {
//         this.name = name
//     }
// }
// ******* EXAMPLE ******* //

const openModalCard = document.getElementById('create')

openModalCard.addEventListener('click', () => {
  // TODO add select , file 3dmodel
  const formCard = new FormCard({ id: 'formCard' })

  const wrapper = document.createElement('div')
  wrapper.className = 'modal__forms'
  wrapper.innerHTML = `<div class="card">
      
        <h2 class="card__role " id="role"></h2>
        <div class="card__3dmodel"></div>
        <div class="card__setting">
          <div>
            <h2 class="card__setting-attackValue " id="attackValue"></h2>
            <img
              class="card__setting-attack"
              src="./assets/img/bluster2.png"
              alt="bluster"
              width="50px"
            />
          </div>
          <h2 class="card__setting-name" id="name"></h2>
          <div>
            <h2 class="card__setting-hpValue" id="hpValue"></h2>
            <img
              class="card__setting-hp"
              src="./assets/img/hp-2.png"
              alt="hp"
              width="50px"
            />
          </div>
        </div>
        <img class="card__rarity" src="./assets/img/common.png" alt="rare" width="80px" />
        <p class="card__spell" id="spell">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus,
          ex?
        </p>
      </div>`
  wrapper.append(formCard.render())

  Modal.handleOpen(wrapper, 'test title')
  const select = createSelect()

  const submit = document.getElementById('saveCard')
  submit.addEventListener('click', e => {
    e.preventDefault()
    formCard.request()
    
  })

  // TODO append select Andrey
  submit.before(select)
  init('../../assets/model/dron/scene.gltf')
})

function createSelect() {
  const valueOptions = ['common', 'rare', 'mythical', 'immortal']
  const gradients = {
    common: {
      backgroundColor: '#b3b3b3',
      backgroundImage: 'linear-gradient(180deg, #b3b3b3 0%, #e7e7e7 100%)'
    },
    rare: {
      backgroundColor: '#74c1eb',
      backgroundImage: 'linear-gradient(180deg, #74c1eb 0%, #9FACE6 100%)'
    },
    mythical: {
      backgroundColor: '#e9dea8',
      backgroundImage: 'linear-gradient(180deg, #e9dea8 0%, #ff3d3d 74%)'
    },
    immortal: {
      backgroundColor: '#FBAB7E',
      backgroundImage: 'linear-gradient(180deg, #FBAB7E 0%, #F7CE68 100%)'
    }
  }
  const select = document.createElement('select')

  select.addEventListener('change', e => {
    const cardRarity = document.querySelector('.card__rarity')
    const card = document.querySelector('.card')

    card.style.cssText = `
    background-color: ${gradients[e.target.value].backgroundColor};
    background-image: ${gradients[e.target.value].backgroundImage};
    `

    cardRarity.src = `./assets/img/${e.target.value}.png`
  })

  valueOptions.forEach(item => {
    const option = document.createElement('option')
    option.value = item
    option.textContent = item
    select.append(option)
  })

  return select
}

// const openModalLogin = document.getElementById('signin')
// const formWrapper = document.querySelector('.forms__wrapper')
// const formSignIn = new FormSignIn({
//   id: 'formSignIn',
//   className: 'forms__wrapper--signin'
// })
// const formSignUp = new FormSignUp({
//   id: 'formSignUp',
//   className: 'forms__wrapper--signup'
// })

// formWrapper.append(formSignIn.render(), formSignUp.render())

// openModalLogin.addEventListener('click', () => {
//   const formSignIn = new FormSignIn({ id: 'formSignIn' })
//   Modal.handleOpen(formSignIn.render(), 'LOGIN')
// })

// const openModalSignUp = document.getElementById('signup')

// openModalSignUp.addEventListener('click', () => {
//   const formSignUp = new FormSignUp({ id: 'formSignUp' })
//   Modal.handleOpen(formSignUp.render(), 'SignUp')
// })

window.addEventListener('load', function () {
  new Modal().render()
})
