import Component from './Component'

export default class Form extends Component {
  handleSubmit() {
    // TODO to the next lesson push data to server
  }

  serialize(selector) {
    const data = {}
    const formFields = document.querySelectorAll(selector)

    formFields.forEach(({ name, value }) => {
      if (value) {
        data[name] = value
      }
    })

    //  const data =  formFields.reduce(
    //     (acc, { name, value }) => ({ ...acc, [name]: value }),
    //     {}
    //   )

    return data
  }

  render(attr) {
    const form = this.createElement('form', attr)
    form.addEventListener('submit', this.handleSubmit)
    this.form = form

    return form
  }
}
