import Component from './Component'
import Card from '../components/Card'

export default class Input extends Component {
  constructor(field) {
    super(field)
    this.props = field
  }

  handleFocus() {
    if (this.error) {
      this.error.remove()
    }
  }

  handleBlur() {
    if (this.value) {
      this.error = this.createElement(
        'p',
        { className: 'error' },
        this.props.errorText
      )
      this.elem.after(this.error)
    }
  }

  handleChange({ target }) {
    Card.setValueCard(target.value, target.name)
  }

  render(isChange = false) {
    const { errorText, label, ...attr } = this.props

    const element = this.createElement('input', attr)

    if (isChange) {
      element.addEventListener('keyup', this.handleChange)
    }

    if (attr.required) {
      element.addEventListener('focus', this.handleFocus.bind(this))
      element.addEventListener('blur', this.handleBlur.bind(this))
    }

    this.elem = element

    if (label) {
      const labelElement = this.createElement('label', label)
      labelElement.prepend(element)
      return labelElement
    } else {
      return element
    }
  }
}
