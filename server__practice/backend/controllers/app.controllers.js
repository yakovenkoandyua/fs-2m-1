import path from 'path'

const __dirname = path.resolve()

const getHomePage = (req, res) => {
	res.sendFile(path.join(__dirname + '../../client/index.html'))
}

const getHomeInfo = (req, res) => {
	res.send(
		JSON.stringify({
			title: 'welcome user',
			desc: 'Сервер выплюнул инфу',
		}),
	)
}

const AppController = {
	getHomePage,
	getHomeInfo,
}

export default AppController
