import { Router } from 'express'
import AppController from '../controllers/app.controllers.js'

const AppRoute = Router()

AppRoute.get('/', AppController.getHomePage)
AppRoute.get('/home', AppController.getHomeInfo)



export default AppRoute
