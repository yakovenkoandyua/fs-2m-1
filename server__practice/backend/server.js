import express, { json, urlencoded } from 'express'


import AppRoute from './routes/app.routes.js'

const app = express()

app.use(json())
app.use(urlencoded({ extended: false }))

app.use('/', AppRoute)

app.listen(8080, () => {
	console.log('Server has been start in port 8080')
})
