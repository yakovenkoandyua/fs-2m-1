/* ЗАДАЧА - 3
 * У нас есть массив участников конкурса (в качестве него может выступать массив clickedLeads из прошлой задачи). По результатам конкурса выиграли только первые четыре участника из массива.
 * НАПИСАТЬ ФУНКЦИЮ, которая принимает аргументом массив участников,
 * после ее вызова на экране должны появиться имена победителей заголовками первого уровня,
 * имена остальный участников на странице должны появиться обычными параграфами.
 * ОБЯЗАТЕЛЬНО ИСПОЛЬЗОВАТЬ ДЕСТРУКТУРИЗАЦИЮ МАССИВОВ В РЕШЕНИИ.
 */

const clickedLeads = [
	{ name: 'John', age: 23 },
	{ name: 'Mia', age: 18 },
	{ name: 'Leo', age: 70 },
	{ name: 'Ed', age: 30 },
	{ name: 'Kiki', age: 27 },
	{ name: 'Otto', age: 20 },
	{ name: 'Ivan', age: 31 },
	{ name: 'Bobbi', age: 30 },
]
// example 1
// function showWinners(winners) {
// 	const [firstWin, secondWin, thirdWin, fourthWin, ...other] = winners
// 	const firstTitle = document.createElement('h1')
// 	const secondTitle = document.createElement('h1')
// 	const thirdTitle = document.createElement('h1')
// 	const fourthTitle = document.createElement('h1')

// 	firstTitle.textContent = firstWin.name
// 	secondTitle.textContent = secondWin.name
// 	thirdTitle.textContent = thirdWin.name
// 	fourthTitle.textContent = fourthWin.name

// 	document.body.prepend(firstTitle, secondTitle, thirdTitle, fourthTitle)

// 	other.forEach(item => {
// 		const text = document.createElement('p')
// 		text.innerText = item.name
// 		document.body.append(text)
// 	})
// }

// showWinners(clickedLeads)

// example 2
function showWinners(winners) {
	const peopleWinner = []
	const otherLeads = []
	for (let i = 0; i < winners.length; i++) {
        let peopleRandom = winners[randomNum()]
		if (i < 4) {
			peopleWinner.push(peopleRandom)
		} else {
			otherLeads.push(peopleRandom)
		}
	}

	peopleWinner.forEach(item => {
		const { name } = item
		const title = createElem('h1', name)
		document.body.prepend(title)
	})

	otherLeads.forEach(({ name }) => {
		const text = createElem('p', name)
		document.body.append(text)
	})
}

const randomNum = () => Math.floor(Math.random() * clickedLeads.length)

function createElem(tag, text) {
	const el = document.createElement(tag)
	el.textContent = text
	return el
}

showWinners(clickedLeads)
