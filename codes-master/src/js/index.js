// * ------------ TABS ------------
const tabsNav = document.querySelector('.tabs-nav'),
	tabsItem = document.querySelectorAll('.tabs-nav_item'),
	tabsContent = document.querySelectorAll('.tabs-content_item');

tabsNav.addEventListener('click', event => {
	if (event.target.classList.contains('tabs-nav')) return null;
	tabsItem.forEach(item => {
		item.classList.remove('active');
	});
	event.target.classList.add('active');
	const itemData = event.target.dataset.title;
	tabsContent.forEach(item => {
		if (item.dataset.content === itemData) {
			item.classList.add('active');
		} else {
			item.classList.remove('active');
		}
	});
});

// const test =  document.querySelector('.tabs-nav_item');
// console.log("=======>", test.getAttribute('data-title'))
// console.log("=======>", test.dataset.title)

// * ------------ MODAL ------------
const viewProfile = document.querySelectorAll('.tabs-content_profile');
const modal = document.querySelector('.modal');
const modalTitle = document.querySelector('.modal-desc_title');
const modalImg = document.querySelector('.modal-body img');
const userName = document.querySelectorAll('.tabs-content_name');
const userImg = document.querySelectorAll('.tabs-content_img');

viewProfile.forEach((element, index) => {
	element.addEventListener('click', () => {
		modal.classList.add('active');
		modalTitle.textContent = userName[index].textContent;
		modalImg.src = userImg[index].src;
	});
});

modal.addEventListener('click', e => {
	// if (!e.target.classList.contains('modal')) return null;
	// modal.classList.remove('active');
	if (e.target.classList.contains('modal')) {
		modal.classList.remove('active');
	}
});
